import constants from './constants'

//export const FETCHING_DATA = 'FETCHING_DATA'
//export const FETCHING_DATA_SUCCESS = 'FETCHING_DATA_SUCCESS'
//export const FETCHING_DATA_FAILURE = 'FETCHING_DATA_FAILURE'

const initialState = {
  data: [],
  dataFetched: false,
  isFetching: false,
  error: false,
  display: "aat",
}

export default function dataReducer (state = initialState, action) {
  switch (action.type) {
    case constants.FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFetching: true
      }
    case constants.FETCHING_DATA_SUCCESS:
      return {
        ...state,
        dataFetched: true,
        isFetching: false,
        data: action.data
      }
    case constants.FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}