import React, { Component } from 'react';
//import React from 'react';
import { connect } from 'react-redux'
import { fetchData } from './actions'

import './AppRedux.css';

class AppRedux extends Component{
  constructor(props) {
    super(props);
    this.state={
      ss: this.props.appData.display
    }
  }
   handleClick(arg){
    this.setState({
       ss : arg
    });
  }
  render(){ 
    return (
      <div>
        <p >Redux Examples</p>
        <button onClick={() => this.props.fetchData()}>
          <p >Load Data</p>
        </button>
        <div>
          {
            this.props.appData.isFetching && <p>Loading</p>
          }
          {
            this.props.appData.data.length ? (
               this.props.appData.data.map((data,index)=> <CreateContentsParent getData1={data} key={index} handleClick={(value)=>this.handleClick(value)} display={this.state.ss}/>)
            ) : null
          }
        </div>
      </div>
    )
  }
}

/*const AppReduxa = (props) => {
  var something= 'Lim OCM'
  function handleClick(arg){
    something = arg
    alert("sini" + something)
  }
  return (
    <div>
      <p >Redux Examples</p>
      <button onClick={() => props.fetchData()}>
        <p >Load Data</p>
      </button>
      <div>
        {
          props.appData.isFetching && <p>Loading</p>
        }
        {
          props.appData.data.length ? (
            props.appData.data.map((data,index)=> <CreateContentsParent getData1={data} key={index} handleClick={(value)=>handleClick(value)} display={something}/>)      
          ) : null
        }
      </div>
    </div>
  )
}*/

const CreateContentsParent = ({getData1, handleClick, display}) => {
  const Contents = getData1.roles.map((data, index) => <CreateContentsChild getData2={data} key={index} getData1={getData1.staffName} display={display}/>);
  return(
    <div>
      <div className="Contents-Parent" onClick={(value)=>handleClick(getData1.staffName)}>
          Name = {getData1.staffName}
      </div>
         {Contents}
    </div>
  )
}

const CreateContentsChild = ({getData2, getData1, display}) => {
  return(
    <div className="Contents-Child">
      <div className={"collapse" + ((display === getData1) ? ' in' : '')}>
       NAME : {getData2.roleId}
      </div>
    </div>
  )
}

function mapStateToProps (state) {
  return {
    appData: state.dataReducer
  }
}

function mapDispatchToProps (dispatch) {
  return {
    fetchData: () => dispatch(fetchData())
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppRedux)