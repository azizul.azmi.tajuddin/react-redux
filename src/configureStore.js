import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

import dataReducer from './dataReducer'

export default function configureStore() {
  let store = createStore(
  	 combineReducers({
	    dataReducer : dataReducer
	    //nak tambah reducers kat sini
	}), applyMiddleware(thunk))
  return store
}