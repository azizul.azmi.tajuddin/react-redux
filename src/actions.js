//import getPeople from './api'
import constants from './constants'

import ajax from 'superagent';
//export const FETCHING_DATA = 'FETCHING_DATA'
//export const FETCHING_DATA_SUCCESS = 'FETCHING_DATA_SUCCESS'
//export const FETCHING_DATA_FAILURE = 'FETCHING_DATA_FAILURE'

export function getData() {
  return {
    type: constants.FETCHING_DATA
  }
}

export function getDataSuccess(data) {
  return {
    type: constants.FETCHING_DATA_SUCCESS,
    data,
  }
}

export function getDataFailure() {
  return {
    type: constants.FETCHING_DATA_FAILURE
  }
}

export function fetchData() {
  return (dispatch) => {
    dispatch(getData())
    getPeople()
      .then((data) => {
        dispatch(getDataSuccess(data))
      })
      .catch((err) => console.log('err:', err))
  }
}

export function getPeople() {
  return new Promise((resolve, reject) => {
    ajax.get('http://mortgage-uat.rhbgroup.com/rhb/api/auth/getStaffList')
    .end((error, response) => {
            if (!error && response) {
                console.log(response.body.data.staffsInfo);
            return resolve(response.body.data.staffsInfo)
            } else {
                console.log('There was an error fetching from GitHub', error);
            }
        }
    )   
  })
}