import React, { Component } from 'react';
import './App.css';
import { render } from 'react-dom'

import PageTwo from './PageTwo';
import PageThree from './PageThree';

export default class PageOne extends React.Component{
  constructor() {
    super();
    this.pergiPageDua = this.pergiPageDua.bind(this);
  }

  functionBalikPageOne(){
      render(
        <PageOne />,
        document.getElementById("root"));    
  }

  functionPergiPageTiga(){
      render(
        <PageThree butangPageTiga={() => {
          this.functionBalikPageOne();
          }} />,
        document.getElementById("root"));
  }

  pergiPageDua(){              
      render(
        <PageTwo butangPageDua={() => {
          this.functionPergiPageTiga();
          }} />,
        document.getElementById("root"));
  }

  render(){ 
     return (
      <div className="App">
        <div className="App-header">
          <h2>PAGE ONE</h2>
          <button onClick={this.pergiPageDua}>GO TO PAGE TWO</button>
        </div>
      </div>
    );
  }
}