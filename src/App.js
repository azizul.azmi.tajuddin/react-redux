import React, { Component } from 'react';
import './App.css';

import { Provider } from 'react-redux'
import configureStore from './configureStore'
import AppRedux from './AppRedux'

export default class App extends Component{

  render(){ 
  const store = configureStore()
     return (
      <div className="App">
        <div className="App-header">
          APP
          <Provider store={store}>
            <AppRedux />
          </Provider>
        </div>
      </div>
    );
  }
}
