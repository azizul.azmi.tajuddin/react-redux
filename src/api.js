import ajax from 'superagent';

export default () => {
  return new Promise((resolve, reject) => {
  	ajax.get('http://mortgage-uat.rhbgroup.com/rhb/api/auth/getStaffList')
  	.end((error, response) => {
            if (!error && response) {
                console.log(response.body.data.staffsInfo);
      			return resolve(response.body.data.staffsInfo)
            } else {
                console.log('There was an error fetching from GitHub', error);
            }
        }
    )   
  })
}